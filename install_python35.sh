#!/bin/bash

sudo apt-get install build-essential libc6-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev -y
cd $HOME
wget https://www.python.org/ftp/python/3.5.2/Python-3.5.2.tgz
tar -zxvf Python-3.5.2.tgz
cd Python-3.5.2
./configure       # 3 min 13 s
# Let's use 4 threads
make -j4          # 8 min 29 s
sudo make install # ~ 4 min
cd ..
sudo rm -fr ./Python-3.5.2*
# upgrade:
sudo pip3 install -U pip
sudo pip3 install -U setuptools

# symlink to new python3.5
sudo rm /usr/bin/python3
sudo ln -s /usr/local/bin/python3.5 /usr/bin/python3
