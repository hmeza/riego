from pi import *
#import wiringpi
from crontab import CronTab
from pi.settings.local import settings


class DuplicatedJobException(Exception):
    pass


class Commander:
    TTL = 10
    TIMEOUT = 3
    WAIT_BETWEEN_REQUESTS = 3
    SERVER = ''
    close = False
    loops = 0

    def __init__(self, server=None, port=80, command_endpoint='api'):
        self.server = server
        self.port = port
        self.ce = command_endpoint
        # if not self.server:
        #     raise Exception("No server to communicate with")
        # log client started

    def main_loop(self):
        while not self.close and not self._timeout():
            response = requests.get(self.SERVER)
            commands = json.dumps(response.content)
            for c in commands.get('command_list'):
                self._execute(c)
            time.sleep(self.WAIT_BETWEEN_REQUESTS)
        # log

    def _execute(self, command):
        try:
            if command.startswith(settings.CUSTOM_COMMAND_PREFIX):
                # riegos alfaro mode
                # configure client, schedule things...
                command = command[len(settings.CUSTOM_COMMAND_PREFIX):]
                import re
                regex = re.compile('(.*)=(.*)')
                match = regex.match(command).groups()
                if match[0] == 'WAIT_TIME':
                    settings.LOOP_TIME = int(match[1])
                    return "Wait time between requests set to {0}".format(match[1])
            if command.startswith('logout') or command.startswith('exit'):
                self.close = True
            else:
                # split command to array using space
                response = subprocess.check_output(command, shell=True, universal_newlines=True)
                return response
        except Exception as e:
            print("Exception executing '{0}'".format(command))
            print(str(e))

    def _timeout(self):
        self.loops += 1
        return self.loops > self.TTL


class Pi:
    """
    Device handler, including execution of commands.
    """
    crontab = []
    results = []
    log = []
    pins = [0] * 50

    def handle_solenoid(self, pin, activate=True):
        import RPi.GPIO as GPIO
        if not pin:
            raise Exception("Pin not specified, aborting solenoid handling")
        print("Version {0}".format(GPIO.VERSION))
        GPIO.setmode(GPIO.BOARD)
        print("Started gpio handler")
        print("setting pin mode as output")
        GPIO.setup(pin, GPIO.OUT)
        print("setting wiringpi pin mode as 1")
        v = GPIO.HIGH if activate else GPIO.LOW
        print("writting {0}".format(str(v)))
        GPIO.output(pin, v)
        print("Pin {0} configured".format(pin))

    def activate_solenoid(self, data):
        """
        Logic: Source is the main water solenoid control. Open source, activate relay for X seconds to power
        it and then deactivate power source for solenoid, so it does not burn up. Then do the same for
        the solenoid.
        :param data:
        :return:
        """
        target_pin = data.get('pin')
        if self.pins[target_pin] == 1:
            return

        # activate SOURCE
        self.handle_solenoid(settings.SOURCE)
        self.add_log(data.get('id'), message='Activated SOURCE')
        # switch source
        self._relay_switch()
        # sleep as much time as specified to wait between source has been opened and solenoid has to be opened
        time.sleep(settings.SOURCE_TIME)
        self.handle_solenoid(target_pin)
        self.add_log(data.get('id'), message='Activated pin {0}'.format(target_pin))
        # switch on solenoid
        self._relay_switch()
        self.pins[target_pin] = 1

    def _relay_switch(self):
        self.handle_solenoid(settings.RELAY)
        time.sleep(settings.RELAY_TIME)
        self.handle_solenoid(settings.RELAY, activate=False)

    def deactivate_solenoid(self, data):
        target_pin = data.get('pin', '')
        if self.pins[target_pin] == 0:
            return
        self.handle_solenoid(target_pin, activate=False)
        self.add_log(data.get('id'), message='Deactivated pin {0}'.format(target_pin))
        # switch solenoid
        self._relay_switch()
        self.pins[target_pin] = 0
        for state in self.pins:
            if state == 1:
                print("Still found an enabled solenoid: {0}".format(self.pins))
                return
        # deactivate source
        self.handle_solenoid(settings.SOURCE, activate=False)
        self._relay_switch()
        self.add_log(data.get('id'), message='Deactivated SOURCE')

    def schedule_solenoid(self, data):
        """
        Append solenoid schedule.
        :param data:
        :return:
        """
        self.crontab.append(data)
        self.add_log(data.get('id'), message='Schedule updated')

    def write_schedule(self):
        """
        Delete and write again crontab to file.
        :return: bool
        """
        cron = CronTab(settings.CRONTAB_USER)
        cron.remove_all()
        if self.crontab:
            for data in self.crontab:
                pin = data.get('pin')
                schedule = data.get('schedule')
                for s in schedule:
                    try:
                        if not s.get('command', None):
                            raise Exception("No command in schedule for solenoid {0} in pin {1}"
                                            .format(data.get('id'), pin))
                        command = "cd /opt/riego; " + settings.PYTHON_PATH + " " + settings.PYTHON_SCRIPT + " " + s.get('command')

                        job = cron.new(command=command)
                        cron_time = "{0} {1} {2} {3} {4}".format(
                            s.get('minute', '*'), s.get('hour', '*'), s.get('day', '*'), s.get('month', '*'), s.get('weekday', '*')
                        )
                        logger.debug("Creating cron for {0} with time {1}".format(command, cron_time))
                        job.setall(cron_time)
                        job.enable()
                    except DuplicatedJobException:
                        pass
                cron.write()
                return True

    def single_command_mode(self, data):
        command_list = data.get('command_list')
        # enter command mode
        # try to retrieve commands each 3 seconds
        # execute and respond
        # keep trying to retrieve commands each 3 seconds until
        # a) command == exit || command == logout
        # b) 10 gets without commands (60 seconds) (configurable)
        commander = Commander()
        for c in command_list:
            self.results.append(
                {"command_id": c.get('id'), "message": commander._execute(c.get('command'))}
            )

    def get_commands_results(self):
        return self.results

    def purge_commands_results(self):
        self.results = []

    def add_log(self, solenoid_id=None, message='', type=1):
        self.log.append({'solenoid_id': solenoid_id, 'message': message, 'type': type})

    def get_log(self):
        if settings.DEBUG:
            # fill some logs
            self.log.append({"message": "Test log"})
            self.log.append({"message": "Test log 2"})
        return self.log

    def purge_log(self):
        self.log = []


class PiMock(Pi):
    """
    Mocked device handler, including execution of commands.
    """

    def handle_solenoid(self, pin, activate=True):
        """
        Nothing but simulation and output.
        :param pin: int
        :param activate: bool
        :return:
        """
        if not pin:
            raise Exception("Pin not specified, aborting solenoid handling")
        print("Version {0}".format('mocked'))
        print("Started gpio handler")
        print("Enabling warnings")
        print("setting pin mode as output")
        print("setting wiringpi pin mode as 1")
        v = 1 if activate else 0
        print("writting {0}".format(str(v)))
        print("Pin {0} configured".format(pin))

    def schedule_solenoid(self, data):
        """
        Append solenoid schedule.
        :param data:
        :return:
        """
        print("Appending this data to crontab: {0}".format(data))
        self.crontab.append(data)
        self.add_log(data.get('id'), message='Schedule updated')

    def write_schedule(self):
        """
        Delete and write again crontab to file.
        :return: bool
        """
        if self.crontab:
            cron = CronTab(settings.CRONTAB_USER)
            cron.remove_all()
            for data in self.crontab:
                pin = data.get('pin')
                schedule = data.get('schedule')
                for s in schedule:
                    try:
                        if not s.get('command', None):
                            raise Exception("No command in schedule for solenoid {0} in pin {1}"
                                            .format(data.get('id'), data.get('pin')))
                        command = settings.PYTHON_PATH + " " + settings.PYTHON_SCRIPT + " " + s.get('command')

                        job = cron.new(command=command)
                        cron_time = "{0} {1} {2} {3} {4}".format(
                            s.get('minute', '*'), s.get('hour', '*'), s.get('day', '*'), s.get('month', '*'), s.get('weekday', '*')
                        )
                        logger.debug("Creating cron for {0} with time {1}".format(command, cron_time))
                        job.setall(cron_time)
                        job.enable()
                    except DuplicatedJobException:
                        pass
                cron.write()
                return True

    def single_command_mode(self, data):
        command_list = data.get('command_list')
        # enter command mode
        # try to retrieve commands each 3 seconds
        # execute and respond
        # keep trying to retrieve commands each 3 seconds until
        # a) command == exit || command == logout
        # b) 10 gets without commands (60 seconds) (configurable)
        commander = Commander()
        for c in command_list:
            self.results.append(
                {"command_id": c.get('id'), "message": commander._execute(c.get('command'))}
            )

    def add_log(self, solenoid_id=None, message='', type=1):
        print({'solenoid_id': solenoid_id, 'message': message, 'type': type})
        self.log.append({'solenoid_id': solenoid_id, 'message': message, 'type': type})
