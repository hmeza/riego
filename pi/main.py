from pi import *
from pi.device import Pi, PiMock
from pi.settings.local import *
import datetime
import logging

# matches functions from pi
RESPONSE_TYPES = ['schedule_solenoid', 'activate_solenoid', 'deactivate_solenoid', 'single_command_mode']

PI_RESERVED_PINS = [1, 2, 4, 6, 8, 9, 10, 14, 17, 20, 25, 27, 28, 30, 34, 39]

class Process:
    def __init__(self):
        self.pi = PiMock() if settings.DEBUG else Pi()  # type: Pi
        # set base pins as OUT
        # start with 36
        for pin in range(1,37):
            if pin in ([settings.RELAY] + PI_RESERVED_PINS):
                continue
            try:
                self.pi.handle_solenoid(pin, activate=False)
                self.pi.pins[pin] = 0
            except Exception as e:
                print(e)
                msg = "Error setting pin {0}".format(pin)
                print(msg)
        # relay always disabled at start
        self.pi.handle_solenoid(settings.RELAY, activate=False)

    def main(self):
        response = None
        server = None
        self.pi.add_log(
            message="Pi started at {0}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        )
        while not self._shutdown():
            try:
                # no cron job. Request to server
                for s in settings.SERVERS:
                    server = s
                    try:
                        self._post(s)
                        response = self._get(s)
                        if response:
                            break
                    except (TimeoutError, ConnectTimeout, Timeout):
                        # log timeout to server, continue with next server
                        self.pi.add_log(message="Unable to connect to server {0}".format(server))
                        continue

                data = json.loads(response.content.decode('utf-8'))

                # validate device_id and signature
                if int(data.get('device_id')) != settings.DEVICE_ID:
                    self.pi.add_log(message="Invalid device_id {0}".format(data.get('device_id')))
                if data.get('signature') != self._get_signature(data.get('random')):
                    self.pi.add_log(message="Invalid signature received from {0}".format(server))
                    raise Exception("Invalid signature received from {0}".format(server))

                self.process(data)
                data = None
            except Exception as e:
                logging.getLogger("pi").debug("Exception {0}".format(str(e)))
                self.pi.add_log(message=str(e))

            logging.getLogger("pi").debug("Sleeping {0}".format(settings.LOOP_TIME))
            time.sleep(settings.LOOP_TIME)

    def _post(self, s):
        key = self._gen_key()
        data = {
            'device_id': settings.DEVICE_ID,
            'random': key,
            'signature': self._get_signature(key),
            'logs': self.pi.get_log() + self.pi.get_commands_results()
        }
        response = requests.post(s, data=json.dumps(data))
        if response.status_code == 200:
            self.pi.purge_commands_results()
            self.pi.purge_log()
        return response

    def _get(self, s):
        random = self._gen_key()
        params = {
           'device_id': settings.DEVICE_ID,
           'random': random,
           'signature': self._get_signature(random)
        }
        response = requests.get(s, params=params, timeout=settings.TIMEOUT)
        return response

    def process(self, data: dict):
        # check if there is a new relay or a new source
        write = False
        if data.get('relay', None) and data.get('relay') != settings.RELAY:
            settings.RELAY = data.get('relay')
            write = True
        if data.get('source', None) and data.get('source') != settings.SOURCE:
            settings.SOURCE = data.get('source')
            write = True
        if write:
            # todo write new config to local.py file
            pass

        self.pi.crontab = []  # each iteration cleans the crontab
        for o in data.get('operations', []):
            function_type = o.get('type', '')
            if function_type in RESPONSE_TYPES:
                getattr(self.pi, function_type)(o)
            else:
                self.pi.log(o.get('id'), message="Not recognized function type {0}".format(function_type))
        self.pi.write_schedule()
        self.pi.purge_commands_results()

    def _shutdown(self):
        return False

    def _gen_key(self):
        import random
        chars = [chr(x) for x in range(ord('0'), ord('9')+1)]\
                + [chr(x) for x in range(ord('A'), ord('Z')+1)]\
                + [chr(x) for x in range(ord('a'), ord('z')+1)]
        chars = ''.join(chars)
        return ''.join(random.choice(chars) for i in range(16))

    def _get_signature(self, random):
        log = logging.getLogger("pi")
        log.debug("sha 256 of {0} {1} {2}".format(str(settings.DEVICE_ID), random, settings.API_CODE))
        log.debug(sha256((str(settings.DEVICE_ID)+random+settings.API_CODE).encode('utf-8')).hexdigest())
        return sha256((str(settings.DEVICE_ID)+random+settings.API_CODE).encode('utf-8')).hexdigest()


def command_line():
    if args[0] != '--pin' or args[2] != '--id' \
            or not ('enable' in args[4] or 'disable' in args[4]):
        raise Exception("Command not recognized")
    data = {
        "device_id": settings.DEVICE_ID,
        "name": "Command line raspberry",
        'operations': [
            {
                "type": "activate_solenoid" if args[4] == "enable" else "deactivate_solenoid",
                "id": int(args[3]),
                "pin": int(args[1]),
            }
        ]
    }
    p.process(data)
    for s in settings.SERVERS:
        try:
            p._post(s)
        except (TimeoutError, ConnectTimeout, Timeout):
            logging.getLogger("pi").debug("Connection timeout to {0}".format(s))


if __name__ == "__main__":
    p = Process()
    args = sys.argv[1:]

    # convert args like --pin=1 activate or --pin=5 deactivate to API data
    if args:
        command_line()
    else:
        p.main()
