from os import path
BASE_DIR = path.dirname(path.dirname(path.dirname(path.abspath(__file__))))


class settings:
    SERVERS = [
    ]

    PYTHON_PATH = '/opt/envs/riego/bin/python'
    PYTHON_SCRIPT = '-m pi.main'

    DEVICE_ID = None
    API_CODE = None
    RELAY = None
    SOURCE = None

    # time between each request to the API
    LOOP_TIME = 60
    # time between opening source and opening the rest of the solenoids
    SOURCE_TIME = 2
    # relay time to power solenoid change
    RELAY_TIME = 0.1

    TIMEOUT = 3

    CRONTAB_USER = 'pi'

    DEBUG = False

    CUSTOM_COMMAND_PREFIX = 'RA:'
