import logging
logger = logging.getLogger('riego')

import os, sys, time, json, subprocess, requests
from requests.exceptions import *
from pi.settings.local import *
from hashlib import sha256, md5
