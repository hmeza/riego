#!/bin/bash

# Freyja client install script
# v.1.05.007
# Hugo Meza <hugo.meza.macias@gmail.com>

# if /opt/riego does not exist...

function clean {
        sudo rm -Rf /opt/riego
        sudo rm -Rf /opt/envs/riego
}

function install {
        # install ssh
        sudo apt-get update
        sudo apt-get install ssh psmisc -y

        sudo chmod 777 /opt

        sudo mkdir ~/.ssh/
        chmod 740 ~/.ssh/

        echo "Host bitbucket.org" >> ~/.ssh/config
        echo "    StrictHostKeyChecking no" >> ~/.ssh/config

        cd /opt
        git clone https://hmeza@bitbucket.org/hmeza/riego.git
        cd /opt/riego
        cp pi/settings/local.py.template pi/settings/local.py

        echo "Installing python 3"
        #/bin/bash /opt/install_python35.sh
        sudo apt-get update
        sudo apt-get install python3 python3-dev python-pip python-software-properties apt-file software-properties-common -y

        echo "Installing Freyja client environment"
        sudo pip install virtualenvwrapper
        source /usr/local/bin/virtualenvwrapper.sh
        mkvirtualenv -p /usr/bin/python3 /opt/envs/riego
        source /opt/envs/riego/bin/activate
        pip install -r pi/requirements.txt

        echo "Installing script in init.d"
        # install script to /etc/init.d and symlink to autostart
        sudo cp /opt/riego/pi/infrastructure/roles/pi/files/riego /etc/init.d/riego
        sudo chmod 755 /etc/init.d/riego
        sudo chown root:root /etc/init.d/riego

        # everything ok, autoinit ssh and riego
        sed -i '$ d' /etc/rc.local
        echo "/etc/init.d/ssh start" >> /etc/rc.local
        echo "/etc/init.d/riego start" >> /etc/rc.local
        echo "START GPRS AT STARTUP? (y/n)?"
        read GPRS_START
        if [[ $GPRS_START = "y" ]]; then
            echo "sleep 60" >> /etc/rc.local
            echo "/sbin/ifdown gprs" >> /etc/rc.local
            echo "/sbin/ifup gprs" >> /etc/rc.local
        fi
        echo "exit 0" >> /etc/rc.local

        # now configure manually
        echo "INSTALL FINISHED - PLEASE ENTER THE FOLLOWING PARAMETERS FOR THE SETTINGS FILE:"
        echo ""
        echo "Device id"
        read DEVICE

        echo ""
        echo "API code"
        read API_CODE

        echo ""
        echo "Source pin"
        read SOURCE_PIN

        echo ""
        echo "Relay pin"
        read RELAY_PIN

        echo ""
        echo "Loop time between requests to the server"
        read LOOP_TIME

        echo "Conf:"
        echo "Device id $DEVICE  API code $API_CODE  Source $SOURCE  Relay $RELAY_PIN  Loop time $LOOP_TIME"

        # replace
        sed -i "s/settings.DEVICE_ID = 1/settings.DEVICE_ID = $DEVICE/g" pi/settings/local.py
        sed -i "s/settings.API_CODE = \"1234\"/settings.API_CODE = \"$API_CODE\"/g" pi/settings/local.py
        sed -i "s/settings.LOOP_TIME = 5/settings.LOOP_TIME = $LOOP_TIME/g" pi/settings/local.py
        sed -i "s/settings.RELAY = 7/settings.RELAY = $RELAY_PIN/g" pi/settings/local.py
        sed -i "s/settings.SOURCE = 8/settings.SOURCE = $SOURCE_PIN/g" pi/settings/local.py
        sed -i "s/settings.DEBUG = True/settings.DEBUG = False/g" pi/settings/local.py

        # finally reboot and check that everything works properly
        echo ""
        echo "Everything OK. Rebooting in 3 seconds..."
        sleep 3
        if [[ `uname -a` = *"orangepi"* ]]; then
            sudo ln -s /usr/lib/klibc/bin/reboot /sbin/reboot
        fi
        sudo reboot
}

if [ -d "/opt/riego" ]; then

        clean
        install
        # restart daemon
        sudo /etc/init.d/riego restart

else

        install

        # finally, start script
        sudo /etc/init.d/riego start

fi
